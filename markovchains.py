#!/bin/python
import random

class TranslationTable:
  table = []
  index = 0
  
  def newEntry(self, token):
    self.table.append(MarkovNode(token))
    self.index += 1
    return (self.index - 1)

  def getEntryID(self, token):
    index = 0
    for i in self.table:
      if i.getToken() == token:
        return index
      else:
          index += 1
    return None
      
  def getNode(self, entry):
    return self.table[entry]

GLOBAL_TABLE = TranslationTable()

class MarkovNode:
  fullSumFrequency = 0
  adjacencyTable = {}
  token = ""  
    
  # t is a token
  def __init__(self, t):
    self.token = t
      
  # enroll a new child node or increment existing one
  def enrollChild(self, id):
    if not id in self.adjacencyTable:
        self.adjacencyTable[id] = 1
    else:
        self.adjacencyTable[id] += 1

  # get token
  def getToken(self):
    return self.token

  # traverse markov graph
  def traverse(self):
    list = sorted([i for i, _ in self.adjacencyTable.items()], key=(lambda x: self.adjacencyTable[x]) )
    n = random.randint(0, self.fullSumFrequency)
    
    for i in list:
      if n < self.adjacencyTable[i]:
        return GLOBAL_TABLE.getNode(i)
      else: 
        n -= self.adjacencyTable[i]

INITIAL_NODE = MarkovNode("")
  
# integrate more text into markov table
def digest(text, tokenizer):
  # tokenize text
  text = tokenizer(text)
  
  # make inital node
  prevNode = INITIAL_NODE
  
  for token in text:
    node = GLOBAL_TABLE.getEntryID(token)
    
    # make node
    if not node:
      node = GLOBAL_TABLE.newEntry(token)
      
    # add node to prev node
    prevNode.enrollChild(node)
   
# traverse current markov model
def read(length, whitespace=""):
  
  # set up loop
  text = "" 
  iterator = INITIAL_NODE
  
  # loop across markov graph
  for n in range(length):
    iterator = iterator.traverse()
    text += iterator.getToken()
    text += whitespace
    
  # return text
  return text

if __name__ == '__main__':
  
  while True:
    print("Welcome to markov shell, type d to digest text and o to output text")
    c = input("  >")
  
    if c == "d":
      text = input(" d>")
      digest(text, (lambda x: x.split(" ")))

    if c == "o":
      len = int(input("How Long? >"))
      print(read(len, " "))
    
    if c == "debug":
      eval(input())  
